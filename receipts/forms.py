from django import forms
from receipts.models import Receipt, ExpenseCategory, Account


class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = (
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(ReceiptForm, self).__init__(*args, **kwargs)
        if user:
            self.fields['category'].queryset = ExpenseCategory.objects.filter(owner=user)
            self.fields['account'].queryset = Account.objects.filter(owner=user)


class CategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ("name",)


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ("name", "number")
